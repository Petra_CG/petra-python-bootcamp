from assignment05.objects import Company, ItCompany
import pytest


@pytest.mark.order(1)
def test_init():
    Company(
        company_name="Capgemini",
        country_of_origin="France",
        headquarter_location="Paris",
        founding_year=1967,
    )


@pytest.mark.order(2)
def test_inheritance():
    company = ItCompany(
        company_name="Capgemini",
        country_of_origin="France",
        headquarter_location="Paris",
        founding_year=1967,
        area="Consulting",
    )
    company.company_name == "Capgemini"


@pytest.mark.order(3)
def test_company_to_str():
    company = ItCompany(
        company_name="Capgemini",
        country_of_origin="France",
        headquarter_location="Paris",
        founding_year=1967,
        area="Consulting",
    )
    assert (
        str(company)
        == "Capgemini, founded in France in 1967 has its headquarter in Paris."
    )


@pytest.mark.order(4)
def test_company_compare():
    company = ItCompany(
        company_name="Capgemini",
        country_of_origin="France",
        headquarter_location="Paris",
        founding_year=1967,
        area="Consulting",
    )
    company_2 = ItCompany(
        company_name="Capgemini Engineering",
        country_of_origin="France",
        headquarter_location="Paris",
        founding_year=1982,
        area="Consulting",
    )
    print(company > company_2)
    assert company > company_2


@pytest.mark.order(5)
def test_global_variable():
    from assignment05 import objects
    print (dir(objects))
    assert "global_count" not in dir(objects)

    objects.CharCounter("abc")
    objects.CharCounter("567")
    assert objects.CharCounter.global_count_as_str() == "Counted 6 chars!"


@pytest.mark.order(6)
def test_property():
    assert (
        Company(
            company_name="Capgemini",
            country_of_origin="France",
            headquarter_location="Paris",
            founding_year=1967,
        ).years_in_business
        == 57
    )


@pytest.mark.order(7)
def test_enum():
    from assignment05.objects import Area
    assert [item for item in Area] == ["Consulting", "Software", "Cloud"]
