
from datetime import datetime
from pydantic import BaseModel, Field, PositiveInt
from datetime import date
class Company:
    # Task 1: Implement an __init__ method for this class
    # Instances of the class should have the attributes: company_name, country_of_origin, headquarter_location, founding_year
    def __init__(self, company_name, country_of_origin, headquarter_location, founding_year):
        self.company_name = company_name
        self.country_of_origin = country_of_origin
        self.headquarter_location = headquarter_location
        self.founding_year = founding_year
    
    def __str__(self):
        return f"{self.company_name}, founded in {self.country_of_origin} in {self.founding_year} has its headquarter in {self.headquarter_location}."
    
    def __lt__(self, other):
        print ("lt")
        # Compare companies based on founding year
        return not (self.founding_year < other.founding_year) # Older companies > younger companies
    
    @property
    def years_in_business(self):
        current_year = datetime.now().year
        return current_year - self.founding_year
    ...
class CompanyPydantic(BaseModel, Company):
    company_name: str
    country_of_origin: str
    headquarter_location: str
    founding_year: PositiveInt = Field(le=datetime.now().year)
    def __str__(self): return Company.__str__(self)
    '''
___str__ aus der Hauptklasse wird nicht automatisch vererbt, da das Pydantic BaseModel vor der CompanyClass steht und daher wird dann deren __str__ bzw. __repr__ Methoden verwendet
Leider kann man das nicht umdrehen, da dann Pydantic meckert
'''




# Task 2: Implement a child class of Company called ItCompany. Implement an __init__ method which additionally sets the attribute: area
# Do not copy the code from Company but call it instead.
class ItCompany(Company):
    def __init__(self, company_name, country_of_origin, headquarter_location, founding_year, area):
        super().__init__(company_name, country_of_origin, headquarter_location, founding_year)
        self.area = area
    
    ...


# Task 3: Implement a method for Company, so that when casting Company to str ( str(Company()) ), the str is: "<company_name>, founded in <country_of_origin> in <founding_year> has its headquarter in <headquarter_location>."

# Task 4: Implement a method for Company, so that companies can be compared to each other. The founding_year attribute should be used for comparison. Older companies > younger companies.


# Task 5: Rewrite this code so that no global variable is altered.
'''
global_count = 0

class CharCounter_original:
    def __init__(self, text: str):
        global global_count
        global_count += len(text)

    @classmethod
    def global_count_as_str(cls):
        return f"Counted {global_count} chars!"
'''
    
class CharCounter:
    total_count = 0  # class variable
    def __init__(self, text: str):
        self._count = len(text)
        CharCounter.total_count += self._count

    @classmethod
    def global_count_as_str(cls):
        return f"Counted {cls.total_count} chars!"

# Bonus: Task 6: Implement an attribute (for Company) that is calculated on the fly when accessed (property decorator).

# The attribute should be called "years_in_business" and should calculate for how many years the company exists


# Bonus: Task 7: Implement an enum called Area which only allows the values: consulting, software and cloud#
from enum import StrEnum, auto

class Area(StrEnum):
    consulting = 'Consulting'
    software = 'Software'
    cloud = 'Cloud'
    def __str__(self):
        return self.name.capitalize()


if __name__ == "__main__":
    r=[item for item in Area]
    print (r)  # Ergebnis: [<Area.consulting: 'Consulting'>, <Area.software: 'Software'>, <Area.cloud: 'Cloud'>]
    print (r==["Consulting", "Software", "Cloud"])  # hier werden die values der enum mit der Liste verglichen
    p=[str(item) for item in Area]
    print (p) # Ergebnis: ['Consulting', 'Software', 'Cloud']

    '''
    x = Company(
        company_name="McDonald",
        country_of_origin="United States",
        headquarter_location="New York",
        founding_year=1955,
    )
    print (x)


    y=CompanyPydantic(
        company_name="Bayer AG",
        country_of_origin="Deutschland",
        headquarter_location="Leverkusen",
        founding_year=1863,
    )
    print (y)
    '''

    '''
    try:
        result = 1 / 0
    except Exception as e:
        print (f"Exception args: {e.args}") 
    '''
    '''

    try:
        company_data= {
        "company_name":"Gibt es noch nicht",
        "country_of_origin":"Deutschland",
        "headquarter_location":"Köln",
        "founding_year":2025}
        company = CompanyPydantic(**company_data)
        print(f"Company data validated successfully: {company}")
    except Exception as e:
        # das liefert alles "ValidationError": type(e).__name__, e.__class__.__name__, e.__class__.__qualname__
        print ("Name of exception", type(e).__name__)
        #print ("message:",getattr(e, 'message',repr(e))) # da es kein message Attribut gibt, wird auch hier e ausgegeben (repl=representation)
        print(f"Ungültige Eingabewerte/Validation Error: {e}")
        print(f"Exception args: {e.args}")  #  anscheinend keine args bei Pydatic exceptions
    
'''