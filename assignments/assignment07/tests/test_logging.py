import pytest

import logging
from assignment07.logging2 import example_function, function_with_import

LOGGER = logging.getLogger(__name__)


@pytest.mark.order(4)
def test_logging(caplog):
    caplog.set_level(logging.INFO)
    example_function()
    assert "I enjoy assignments" in caplog.text
    caplog.clear()
    caplog.set_level(logging.WARNING)
    example_function()
    assert "I enjoy assignments" not in caplog.text
    assert "Really!" in caplog.text


@pytest.mark.order(5)
def test_logging_config(caplog):
    function_with_import()
    with open("log.log") as fh:
        logged_text = fh.read()
    assert "Starting logs_lots" in logged_text
    assert "message1" not in logged_text
    assert "message2" in logged_text
