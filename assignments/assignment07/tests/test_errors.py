from assignment07.errors import catch_me_if_you_can, final_file, NoneKeyException
import pytest
import os


@pytest.mark.order(1)
def test_catch_me_one():
    assert catch_me_if_you_can({"a": "b"}, "a") == "b"
    assert catch_me_if_you_can({"a": "b"}, "c") == "Did not find c"


@pytest.mark.order(2)
def test_catch_me_two():
    with pytest.raises(NoneKeyException):
        catch_me_if_you_can({"a": "b"}, None)


@pytest.mark.order(3)
def test_final_file():
    filename = "test_file"

    def success():
        assert os.path.exists(filename)
        return None

    def error():
        raise KeyError

    final_file(success)
    assert not os.path.exists(filename)
    try:
        final_file(error)
    except Exception:
        pass
    finally:
        assert not os.path.exists(filename)
