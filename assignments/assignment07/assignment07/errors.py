from typing import Callable
import contextlib
import os


class NoneKeyException(Exception):
    pass


def catch_me_if_you_can(data: dict[str, str], key: str | None) -> str:
    # Task 1: Catch KeyErrors and return f"Did not find {key}" instead.
    # Task 2: If key is None, return a NoneKeyException
    if key is None:
        raise NoneKeyException
    else:
        try:
            return data[key]
        except:
            # raise KeyError(f"Did not find {key}")
            return (f"Did not find {key}")
    ...

def new_func():
    pass
    ...
# Task 3: Create a file "test_file" and ensure it is always delete after function was called
def final_file(function: Callable) -> None:
    filename = "test_file"
    try:
        with open(filename, "w") as file:
            yield file
    except Exception as e:
        print(f"Error creating file: {e}")

    try:
        function()
    except Exception:
        print (f"Error calling function {function}")
    finally:
        # Delete the file after the context is exited
        os.remove(filename)
    ...



if __name__ == "__main__":
 #   result = catch_me_if_you_can ({"1":"Hallo Welt", "2":"nix"},"1")
 #   print (result)
 #   result = catch_me_if_you_can ({"1":"Hallo Welt", "2":"nix"},None)
 #   result = catch_me_if_you_can ({"1":"Hallo Welt", "2":"nix"},"3")
 final_file(new_func)
  