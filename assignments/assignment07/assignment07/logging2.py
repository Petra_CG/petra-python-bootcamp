import logging
import logging.config
from assignment07.lib import logs_lots
LOGGING_CONFIG = {
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'standard': {
            'format': '%(asctime)s [%(levelname)s] %(name)s: %(message)s'
        },
    },
    'handlers': {
        'default': {
            'level': 'DEBUG',  # Set to DEBUG to log debug messages
            'formatter': 'standard',
            'class': 'logging.StreamHandler',
            'stream': 'ext://sys.stdout',  # Default is stderr
        },
        'file_handler': {
            'level': 'DEBUG',  # Set to DEBUG to log debug messages
            'formatter': 'standard',
            'class': 'logging.FileHandler',
            'filename': 'log.log',  # Log to a file named 'log.log'
        },
    },
    'loggers': {
        '': {  # root logger
            'handlers': ['file_handler'],  # Add the file handler to the root logger
            'level': 'DEBUG',  # Set root logger level to DEBUG
            'propagate': False
        },
        'lib': {
            'handlers': ['file_handler'],  # Add the file handler to the 'lib' logger
            'level': 'INFO',  # Set lib logger level to INFO
            'propagate': False
        },
        '__main__': {  # if __name__ == '__main__'
            'handlers': ['default'],
            'level': 'DEBUG',
            'propagate': False
        },
    }
}



# Task 4: Implement example_function which logs 'I enjoy assignments' with level INFO and 'Really!' at level WARNING
def example_function(): 
    # Create a logger
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.INFO)
    # Create a console handler and set the level to INFO
    ch = logging.StreamHandler()
    ch.setLevel(logging.INFO)
    # Add the console handler to the logger
    logger.addHandler(ch)

    # Log the messages
    logger.info("I enjoy assignments")
    logger.warning("Really!")
    
    ...


# Task 5: Create a logging configuration within function_with_import for the lib logger. For lib logger, set INFO level. For root logger, set DEBUG level.
# Create and use a logging.FileHandler handler for lib and root logger and write to log.log.
def function_with_import():
    # load config
    logging.config.dictConfig(LOGGING_CONFIG)
    logging.debug("Starting logs_lots")
    logs_lots()






def sample_function2():
    # Create a logger
    logger = logging.getLogger("Jupyterhub")
    logging.basicConfig(level=logging.INFO)

    # Log messages via dedicated logger Jupyterhub
    logger.info("I enjoy assignments")
    logger.warning("Really!")


    # Log messages via root logger
    logging.info("I enjoy assignments")
    logging.warning("Really!")


if __name__ == "__main__":
    function_with_import()