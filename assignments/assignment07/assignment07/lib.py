import logging

logger = logging.getLogger("lib")


def logs_lots():
    logger.debug("message1")
    logger.info("message2")
    logger.warning("message3")
    logger.error("message4")
    logger.critical("message5")
