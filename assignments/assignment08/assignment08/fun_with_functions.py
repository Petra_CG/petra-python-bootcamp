# -*- coding: utf-8 -*-
from functools import reduce


def fibonacci_recursive(n: int) -> int:
    """Task01
    Returns the n-th element of the fibonacci sequence
    Use recursion to calculate the value

    Args:
        n (int): n-th element of fibonacci sequence, must be > 0 
    Returns:
        int: integer value from the fibonacci sequence, return -1 when n is <=0
    """
    if n <= 0:
        return -1
    elif n == 1:
        return 0
    elif n == 2:
        return 1
    else:
        return fibonacci_recursive(n - 1) + fibonacci_recursive(n - 2)
    ...

def memorize(to_memorize: callable) -> callable:
    """Task02
    Write a memorize function es described in the link below
    https://gist.github.com/oskarkv/3168ea3f8d7530ccd94c97c19aafe266#memoize
    For testing purposes you are required to expose the memory dict of the returned function.
    The required code for this is given below. 

    Hint 1: You only need to handle an undefined list of positional arguments for the to_memorize function.
    Hint 2: Remember that *args as parameter is a tuple when used without the astersik.
    Hint 3: When you want to pass *args to another function do not forget the asterisk 

    Args:
        to_memorize (callable): any function which takes a undefined list of positional arguments

    Returns:
        callable: a function which calls will be memorized so that a call with the same parameters is not computed a second time.
                    the function has an attribute 'memory' which exposes the memorized parameters and results 
    """

    memory: dict = {}

    def RENAME_ME():
        ...

    RENAME_ME.memory = memory

    ...
def memorize(to_memorize):
    """
    Memoizes a given function by caching the computed result.
    
    Args:
        to_memorize (callable): Any function which takes an undefined list of positional arguments.
    
    Returns:
        callable: A function which calls will be memorized so that a call with the same parameters is not computed a second time.
                  The function has an attribute 'memory' which exposes the memorized parameters and results.
    """
    memory = {}  # Dictionary to store computed results
    
    def memoized_function(*args):
        if args in memory:
            # If the result is already in memory, return it
            return memory[args]
        else:
            # Otherwise, compute the result and store it in memory
            result = to_memorize(*args)
            memory[args] = result
            return result
    
    # Attach the memory dictionary to the memoized function
    memoized_function.memory = memory
    return memoized_function

# Example usage:
def add(a, b):
    return a + b

# Usecase:
@memorize
def add(a, b):
    return a + b
## is equivalent to
def unmemorized_add(a, b):
    return a + b
memorized_add = memorize(unmemorized_add)

def write_a_generator():
    """Task (optional)
    Write a generator
    """

    ...

def foo(x): 
    def bar(y): 
        return x + y
    return bar
    



if __name__ == "__main__":
    # Test the function
   # n = 10
  #  result = fibonacci_recursive(n)
   # print(f"The {n}-th element of the Fibonacci sequence is: {result}")
   # Create a memoized version of the add function
  #  memoized_add = memorize(add)

    # Call the memoized function
 #   print(memoized_add(2, 3))  # Computes and caches the result
  #  print(memoized_add(2, 3))  # Retrieves the cached result
  #  print(memoized_add.memory)  # Exposes the memorized parameters and results
    
    baz = foo(10) 
    baz(32) 
    baz2 = foo(0) 
    baz(32) 
    baz(32) 