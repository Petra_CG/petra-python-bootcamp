# -*- coding: utf-8 -*-

import pytest
import httpx
import uuid
from assignment04.webserver import get_tree, add_tree, update_tree, delete_tree

tree1 = {"tree_id": str(uuid.uuid1()), "tree_name": "Eiche",
         "botanical_name": "Quercus", "age": 99}
tree1_update = {"tree_id": tree1['tree_id'],
                "tree_name": "Andere Eiche", "botanical_name": "Quercus", "age": 42}
tree2 = {"tree_id": str(uuid.uuid1()), "tree_name": "No one knows what it's like To be the bad man To be the sad man Behind blue eyes And no one knows what it's like To be hated To be fated to telling only lies",
         "botanical_name": "Quercus", "age": 99}
tree3 = {"tree_id": str(uuid.uuid1()), "tree_name": "Eiche",
         "botanical_name": "Quercus", "age": -5}


@pytest.mark.order(1)
def test_add_tree():
    r = httpx.post('http://localhost:8000/tree/', json=tree1)
    r.status_code == httpx.codes.OK
    print (r.json())
    r.json()['tree_id'] == tree1['tree_id']
    r.json()['tree_name'] == tree1['tree_name']
    r.json()['botanical_name'] == tree1['botanical_name']
    r.json()['age'] == tree1['age']


@pytest.mark.order(2)
def test_update_tree():
    r = httpx.put(f'http://localhost:8000/tree/{tree1['tree_id']}', json=tree1_update)
    r.status_code == httpx.codes.OK


@pytest.mark.order(3)
def test_get_tree():
    r = httpx.get(f'http://localhost:8000/tree/{tree1['tree_id']}')
    r.status_code == httpx.codes.OK
    r.json()['tree_id'] == tree1_update['tree_id']
    r.json()['tree_name'] == tree1_update['tree_name']
    r.json()['botanical_name'] == tree1_update['botanical_name']
    r.json()['age'] == tree1_update['age']


@pytest.mark.order(4)
def test_delete_tree():
    r = httpx.delete(f'http://localhost:8000/tree/{tree1['tree_id']}')
    r.status_code == httpx.codes.OK


@pytest.mark.order(5)
def test_get_tree_not_found():
    r = httpx.get(f'http://localhost:8000/tree/{tree1['tree_id']}')
    r.status_code == httpx.codes.NOT_FOUND


@pytest.mark.order(6)
def test_delete_tree_not_found():
    r = httpx.delete(f'http://localhost:8000/tree/{tree1['tree_id']}')
    r.status_code == httpx.codes.NOT_FOUND


def test_add_tree_too_long_name():
    r = httpx.post('http://localhost:8000/tree/', json=tree2)
    r.status_code == httpx.codes.BAD_REQUEST


def test_add_tree_negative_age():
    r = httpx.post('http://localhost:8000/tree/', json=tree3)
    r.status_code == httpx.codes.BAD_REQUEST


# def test_test():
#     r = httpx.get('http://localhost:8000/')
#     r.status_code == httpx.codes.OK
#     r.text == "Hello World"