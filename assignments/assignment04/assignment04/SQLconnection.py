import sqlite3
import uuid

def dict_factory(cursor, row):
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d


def create_connection_and_table():
    # create an in memory sqlite database
    sql_connection = sqlite3.connect(':memory:')
   # sql_connection.row_factory = sqlite3.Row  
    cur = sql_connection.cursor()
    cur.execute("CREATE TABLE trees(treeId varchar(36),treeName varchar(255),botanicalName varchar(255),age int)")
    return cur