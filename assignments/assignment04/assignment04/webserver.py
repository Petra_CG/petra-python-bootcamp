
# https://fastapi.tiangolo.com/#installation


from typing import Annotated
import uuid
import json
from fastapi import Body, FastAPI, Response
from pydantic import BaseModel, Field

from . import sql_connection

app = FastAPI()

print(type(sql_connection))
cur = sql_connection.cursor()

class Tree(BaseModel):
    """
    create a Tree data model with pydantic and the below described Attributes

    Attributes
    ----------
    tree_id : uuid.UUID
        uuid of the UUID
    tree_name : str | None
        the common name of the tree
    botanical_name : str | None
        botanical name of the tree 
        string with max 100 characters or None 
    age : int | None
        must be greater than 0
    """
    tree_id : uuid.UUID
    tree_name: str| None = Field(description="the common name of the tree",title="tname")
    botanical_name: str | None = Field(max_length=100, description="botanical name of the tree")
    age: int | None= Field(gt=0, description="Age must be greater than 0")
    ...
"""
GET : Get object
PUT : Modify Object
DELETE: Delete Object
POST : Create Object
https://fastapi.tiangolo.com/tutorial/body-fields/
"""
@app.get("/")
def read_root():
    #list all records in table trees
    cur = sql_connection.cursor()
    # data = {"treeId": str(uuid.uuid1()), "treeName": "Eiche", "botanicalName": "Quercus", "age": 99}
    # cur.execute("INSERT INTO trees VALUES(:treeId, :treeName, :botanicalName, :age)", data)
    cur.execute("SELECT * FROM trees")
    # print(cur.fetchall())  
    return cur.fetchall()

@app.get("/tree/{tree_id}")
def get_tree(tree_id: uuid.UUID, response: Response):
    """
    Expose this function as GET request with FastAPI under the path /tree

    Args:
        Query Paramters
            tree_id: uuid.UUID

    Returns
        Http Status Code
            200: if entry was found
            404: if the entry was not found
            400: else
        Body:
            the new Tree object as JSON
    """
    '''Lösung mit ?
    query = "SELECT * FROM trees WHERE treeId = ?"
    params = (str(tree_id),)  # Pass the parameter as a tuple
    '''
    query = "SELECT * FROM trees WHERE treeId = :tree_id"
    params = {'tree_id': str(tree_id)}  # Pass the parameters as a dictionary
    resJSON = {}
    try:
        cur=sql_connection.cursor()
        cur.execute(query, params)  # Execute the query with the parameter
        result = cur.fetchall()
        print (result)
        if len(result) == 0:
            response.status_code = 404 # no entry found
            resJSON = {"Error":"no entry found"}
        elif len(result) > 1:
            response.status_code = 400 # more than one entry found
            resJSON = {"Error":"more than one entry found"}
        else:
            response.status_code = 200
            tree_object = Tree(tree_id=result[0][0], tree_name=result[0][1], botanical_name=result[0][2], age=result[0][3])
            resJSON = tree_object.model_dump()  # alt. tree_object.json()   
    except:
        response.status_code = 400
        resJSON = {"Error":"general error reading record"}
    finally:
        cur.close()  # cursor schließen 
    return  resJSON
    ...

@app.post("/tree/")
def add_tree(tree_body: Annotated[Tree,Body(embed=False)], response: Response=400):
    """
    Expose this function as POST request with FastAPI under the path /tree
    Note: the tree_name is not required to be unique in the db

    Args:
        Query Paramters
            
        Body:
            inserted Tree object as JSON

    Returns
        Http Status Code
            200: if entry was successfully created
            400: else
        Body:
            the new Tree object as JSON
    """
    resJSON ={}
    #model_dump {'tree_id': UUID('3fa85f64-5717-4562-b3fc-2c963f66afa6'), 'tree_name': 'neuer Baum', 'botanical_name': 'keine Ahnung', 'age': 166}
    data = tree_body.model_dump() # Klasse =>Json
    data['tree_id']=str(data['tree_id']) # da in der Klasse ID UUID ist und in der Datenbank str
    print ("data",data)
    query = "INSERT INTO trees  (treeId, treeName, botanicalName, age) VALUES(:tree_id, :tree_name, :botanical_name, :age)"
    try:
        cur=sql_connection.cursor()
        cur.execute(query, data)  # Execute the query with the parameter
        response.status_code = 200
        # resJSON = {**{"OK":"record sucessfully inserted"},**data}
        resJSON = data
    except:
        response.status_code = 400
        resJSON = {"Error":"general error inserting record"}
    finally:
        cur.close()  # cursor schließen 
    return resJSON
    ...

@app.put("/tree/{tree_id}")
def update_tree(tree_id: uuid.UUID, tree_body: Annotated[Tree,Body(embed=False)], response: Response):
    """
    Expose this function as PUT request with FastAPI under the path /tree

    Args:
        Query Paramters
            tree_id: uuid.UUID
        Body:
            updated Tree object as JSON

    Returns
        Http Status Code
            200: if entry was successfully updated
            404: if the entry was not found in the database
            400: else
        Body:
            empty
    """
    # UPDATE table_name SET column_1 = new_value_1, column_2 = new_value_2 WHERE search_condition;
    query_search = "SELECT * FROM trees WHERE treeId = :param_tree_id"
    query_update = "UPDATE trees SET 'treeId' = :tree_id, 'treeName' = :tree_name, 'botanicalName' = :botanical_name, 'age' = :age WHERE treeId = :param_tree_id"
    # tree_body_json = json.loads(tree_body)
    data = tree_body.model_dump() # Klasse =>Json
    data['tree_id']=str(data['tree_id']) # da in der Klasse ID UUID ist und in der Datenbank str
    params = {**data,**{'param_tree_id': str(tree_id)}}  # Query param tree_id
    resJSON = {}
    try:
        cur=sql_connection.cursor()
        cur.execute(query_search, params)  # search tree_id
        result = cur.fetchall()
        if len(result) == 0:
            response.status_code = 404 # no entry found
            resJSON = {"Error":"no entry found"}
        else:
            try:
                cur.execute(query_update, params)  # update tree
                response.status_code = 200
                resJSON = {"OK":"record sucessfully updated"}
            except:
                response.status_code = 400
                resJSON = {"Error":"general error updating record"}
    except:
        response.status_code = 400
        resJSON = {"Error":"general error reading database"}
    finally:
        cur.close()  # cursor schließen 
    return  resJSON
    ...

@app.delete("/tree/{tree_id}")
def delete_tree(tree_id: uuid.UUID, response: Response):
    """
    Expose this function as DELETE request with FastAPI under the path /tree

    Args:
        Query Paramters
            tree_id: uuid.UUID

    Returns:
        Http Status Code
            200: if entry was successfully deleted
            404: if the entry was not found in the database
            400: else
        Body:
            empty
    """
    resJSON = {}
    query_delete = "DELETE FROM trees WHERE treeId = :tree_id"
    query_search = "SELECT * FROM trees WHERE treeId = :tree_id"
    params = {'tree_id': str(tree_id)}  # Pass the parameters as a dictionary
    try:
        cur=sql_connection.cursor()
        cur.execute(query_search, params)  # search tree_id
        result = cur.fetchall()
        if len(result) == 0:
            response.status_code = 404 # no entry found
            resJSON = {"Error":"no entry found"}
        elif len(result) > 1:
            response.status_code = 400 # more than one entry found
            resJSON = {"Error":"more than one entry found"}
        else:
            try:
                cur.execute(query_delete, params)  # update tree
                response.status_code = 200
                resJSON = {"OK":"record sucessfully deleted"}
            except:
                response.status_code = 400
                resJSON = {"Error":"general error deleting record"}
    except:
        response.status_code = 400
        resJSON = {"Error":"general error reading database"}
    finally:
        cur.close()  # cursor schließen 
    return  resJSON  
    ...
