from dotenv import load_dotenv
import os

load_dotenv()

username = os.getenv('MY_USERNAME')
password = os.getenv('MY_PASSWORD')

print (f"Benutzername: {username} und Passwort: {password}")