def find_nearest_value(lst, target):
    """
    Findet den nächstgelegenen Wert in einer Liste zum Zielwert.
    Wenn es zwei nächstgelegene Werte gibt, wird der größere zurückgegeben.
    """
    lst.sort()  # Liste aufsteigend sortieren
    nearest = None

    for val in lst:
        if nearest is None or abs(val - target) < abs(nearest - target):
            nearest = val
        elif abs(val - target) == abs(nearest - target):
            nearest = max(nearest, val)  # Bei gleicher Entfernung den größeren Wert nehmen

    return nearest

# Beispielverwendung
meine_liste = [10, 20,26, 30, 40, 50]
zielwert = 25
ergebnis = find_nearest_value(meine_liste, zielwert)
print(f"Der nächstgelegene Wert zu {zielwert} in der Liste ist {ergebnis}.")
