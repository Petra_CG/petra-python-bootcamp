from kubernetes import client, config

def delete_custom_object(api_instance, name, namespace):
    try:
        api_instance.delete_namespaced_custom_object(
            group="capgemini.com",
            version="v1",
            plural="plants",
            name=name,
            namespace=namespace,
        )
        print(f"Custom object '{name}' deleted successfully.")
    except client.rest.ApiException as e:
        print(f"Error deleting custom object: {e}")

def create_custom_object(api_instance, name, namespace):
    custom_object = {
        "apiVersion": "capgemini.com/v1",
        "kind": "Plant",
        "metadata": {"name": name},
        "spec": {
            # Define your custom object fields here
            "petName": "dfgsdfgdsf",
            "sunIntensity": 1,
        },
    }
    try:
        api_instance.create_namespaced_custom_object(
            group="capgemini.com",
            version="v1",
            plural="plants",
            namespace='balkony',
            body=custom_object,
        )
        api_instance.create_namespaced_custom_object(
            group="capgemini.com",
            version="v1",
            plural="plants",
            namespace='office',
            body=custom_object,
        )
        print(f"Custom object '{name}' created in namespace '{namespace}'.")
    except client.rest.ApiException as e:
        print(f"Error creating custom object: {e}")






def change_namespace_of_crd(api_instance):

    # Parameter für die CRD
    GROUP = 'capgemini.com'  
    VERSION = 'v1'  
    PLURAL = 'plants'  
    NAME = 'rosemary'  # Name des CRD-Objekts
    CURRENT_NAMESPACE = 'default'  # aktueller Namespace
    NEW_NAMESPACE = 'balkony'  # neuer Namespace

    # Das bestehende Objekt abrufen
    existing_object = api_instance.get_namespaced_custom_object(
        group=GROUP,
        version=VERSION,
        namespace=CURRENT_NAMESPACE,
        plural=PLURAL,
        name=NAME
    )

    # Das Objekt im neuen Namespace erstellen
    # 'metadata' und andere nötige Felder setzen
    existing_object['metadata']['namespace'] = NEW_NAMESPACE
    if 'resourceVersion' in existing_object['metadata']:
        del existing_object['metadata']['resourceVersion']
    if 'uid' in existing_object['metadata']:
        del existing_object['metadata']['uid']
        
    api_instance.create_namespaced_custom_object(
        group=GROUP,
        version=VERSION,
        namespace=NEW_NAMESPACE,
        plural=PLURAL,
        body=existing_object
    )

    # Das Objekt im alten Namespace löschen
    api_instance.delete_namespaced_custom_object(
        group=GROUP,
        version=VERSION,
        namespace=CURRENT_NAMESPACE,
        plural=PLURAL,
        name=NAME
    )

    print(f"CRD object {NAME} moved from namespace {CURRENT_NAMESPACE} to {NEW_NAMESPACE}")


def main():
    config.load_kube_config()  # Load kubeconfig from default location
    api_instance = client.CustomObjectsApi()
    # change_namespace_of_crd(api_instance)
    create_custom_object(api_instance, "petunie", "balkony")

    '''    
    # Specify the custom object name and namespaces
    custom_object_name = "rose2"
    current_namespace = "default"
    new_namespace = "office"

    # Delete the custom object in the current namespace
   # delete_custom_object(api_instance, custom_object_name, current_namespace)

    # Recreate the custom object in the new namespace
    create_custom_object(api_instance, custom_object_name, new_namespace)
    '''

if __name__ == "__main__":
    main()
