'''Um einen Kubernetes-Controller zu erstellen, der einem CR Plant einen Namespace zuweist und eine Annotation hinzufügt, können Sie die folgenden Schritte ausführen:

Controller-Struktur:
1. Erstellen Sie eine Python-Datei (z. B. plant_controller.py), die Ihren Controller enthält.
2. Importieren Sie die erforderlichen Kubernetes-Bibliotheken (z. B. kubernetes.client).
3. Definieren Sie Ihre Funktionen zum Zuweisen des Namensraums und Hinzufügen der Annotation.
4. CRD-Beobachtung:
 -  Verwenden Sie die watch-Funktion, um Änderungen an den CR Plants zu überwachen.
 -  Wenn ein neuer CR Plant erstellt wird, rufen Sie Ihre Funktionen auf, um den Namespace zuzuweisen und die Annotation hinzuzufügen.
5. Namespace-Zuweisung:
 -  Verwenden Sie die patch_namespaced_custom_object-Funktion, um den Namespace für den CR Plant zu ändern.
 -  Beispiel: custom_api.patch_namespaced_custom_object(group, version, namespace, plural, name, {"metadata": {"namespace": namespace}})
6. Annotation hinzufügen:
 -  Verwenden Sie dieselbe Funktion, um eine Annotation hinzuzufügen.
 -  Beispiel: custom_api.patch_namespaced_custom_object(group, version, namespace, plural, name, {"metadata": {"annotations": {"my-annotation": "my-value"}}})
'''

from kubernetes import client, config, watch
def main():
    # Load Kubernetes configuration
    config.load_kube_config()
    api = client.CustomObjectsApi()

    # Define the API group, version, and resource type
    group = "example.com"
    version = "v1"
    resource = "databasebackups"