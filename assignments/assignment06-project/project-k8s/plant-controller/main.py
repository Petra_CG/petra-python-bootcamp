# -*- coding: utf-8 -*-
'''
from fastapi import FastAPI
from kubernetes import client, config
from pathlib import Path

app = FastAPI()

@app.get("/")
def hello_world():
    return "hello world"


# Configs can be set in Configuration class directly or using helper utility
# Example for further configuration: config.load_kube_config(config_file=str(Path("C:/Users/mroenfel/.kube/config")), context="rancher-desktop")
# If you have more than one cluster in your kube config make sure to set the context
config.load_kube_config(context="rancher-desktop")



'''

'''-----------------------------------------------------'''
from kubernetes import client, config
import datetime
import logging
import sys


# Load Kubernetes configuration
config.load_kube_config()

# Create a logger
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
formatter = logging.Formatter('%(asctime)s [%(levelname)s] %(name)s: %(message)s')
# Create a console handler and set the level to INFO
ch = logging.StreamHandler()
ch.setLevel(logging.INFO)
ch.setFormatter(formatter)
# Add the console handler to the logger
logger.addHandler(ch)

# Initialize Kubernetes API client
v1 = client.CoreV1Api()
custom_api = client.CustomObjectsApi()


# Define the CRD group, version, and plural
GROUP = "capgemini.com"
VERSION = "v1"
PLURAL = "plants"

# Define the labels for sun intensity
SUN_INTENSITY_LABELS = {}

def initialize_sun_intensity_labels():
    """
    Initialize SUN_INTENSITY_LABELS from existing namespaces.
    """
    try:
        namespaces = v1.list_namespace()
        for ns in namespaces.items:
            sun_intensity = ns.metadata.labels.get("sunIntensity")
            if sun_intensity:
                SUN_INTENSITY_LABELS[sun_intensity] = ns.metadata.name
    except Exception as e:
        logger.error("Kubernetes namespaces cannot be determined - Exiting the program.../ Error: %s", e)
        sys.exit(0)


def find_nearest_value(lst, target):
    """
    Findet den nächstgelegenen Wert in einer Liste zum Zielwert.
    Wenn es zwei nächstgelegene Werte gibt, wird der größere zurückgegeben.
    """
    lst.sort()  # Liste aufsteigend sortieren
    nearest = None
    for val in lst:
        if nearest is None or abs(int(val) - int(target)) < abs(nearest - int(target)):
            nearest = int(val)
        elif abs(int(val) - int(target)) == abs(nearest - int(target)):
            nearest = max(nearest, int(val))  # Bei gleicher Entfernung den größeren Wert nehmen

    return str(nearest)

def get_sun_intensity(plant):
    """
    Get the sun intensity label for the given plant.
    """
    sun_intensity = str(plant["spec"].get("sunIntensity"))
    if sun_intensity not in SUN_INTENSITY_LABELS.keys():
        sun_intensity = find_nearest_value(list(SUN_INTENSITY_LABELS.keys()), sun_intensity)

    if sun_intensity:
        return SUN_INTENSITY_LABELS.get(sun_intensity)
    else:
        logger.info("No sun intensity specified for plant %s",plant['metadata']['name'])
        return None
    
def move_plant_to_namespace(plant, target_namespace):
    """
    Move the plant to the target namespace.
    Um den Namespace eines Kubernetes Custom Resource Definition (CRD) Objekts zu ändern, ist das direkte Aktualisieren des Namespace-Feldes 
    über eine replace_namespaced_custom_object Methode nicht ausreichend. 
    Der Namespace eines Objekts ist ein immutables Feld, das beim Erstellen festgelegt wird und nicht geändert werden kann.
    Stattdessen musst das CRD-Objekt im alten Namespace gelöscht und im neuen Namespace neu erstellen.
    hilfreiche Befehle
    kubectl get plants --all-namespaces
    kubectl get namespaces
    kubectl delete plants petunie -n office  plant in einem nicht default namespace löschen
    """
    plant_name = plant["metadata"]["name"]
    current_namespace = plant["metadata"]["namespace"]

    # Update the plant's namespace
    plant["metadata"]["namespace"] = target_namespace
    # remove UUID fields
    if 'resourceVersion' in plant['metadata']:
        del plant['metadata']['resourceVersion']
    if 'uid' in plant['metadata']:
        del plant['metadata']['uid']

    logger.info ("Plant %s currently located in %s will be moved to target location %s" % (plant_name, current_namespace, target_namespace))
    try:
        #create plant n new namespace
        newobject=custom_api.create_namespaced_custom_object(
            group=GROUP,
            version=VERSION,
            namespace=target_namespace,
            plural=PLURAL,
            body=plant
        )
        #remove plant from old namespace
        custom_api.delete_namespaced_custom_object(
            group=GROUP,
            version=VERSION,
            namespace=current_namespace,
            plural=PLURAL,
            name=plant_name,
        )
        logger.info(f"Moved plant {plant_name} to namespace {target_namespace}")
    except Exception as e:
        logger.error("Kubernetes namespaces cannot be switched - processing of this plant is stopedclear/ Error: %s", e)
        return
        
    return newobject
  

def main():
    # print("Listing pods with their IPs:")
    # ret = v1.list_pod_for_all_namespaces(watch=False)
    # for i in ret.items:
    #    print("%s\t%s\t%s" % (i.status.pod_ip, i.metadata.namespace, i.metadata.name))
    # print('==============================================================================')

    # Initialize SUN_INTENSITY_LABELS
    initialize_sun_intensity_labels()
    try:
        all_crd_objects = custom_api.list_cluster_custom_object(
            group=GROUP,
            version=VERSION,
            plural=PLURAL
        )['items']
    except Exception as e:
        logger.error ("Kubernetes plant CRD could not be determined / Error: %s",e)
        sys.exit(0)
            
    if len(all_crd_objects) == 0:
        logger.info("No plants available")
    else:
        for plant in all_crd_objects:
                logger.info ("plant: %s", plant["metadata"]["name"])
                sun_intensity_namespace = get_sun_intensity(plant)
                logger.info("needed location/sun intensity: %s" , sun_intensity_namespace)
                if sun_intensity_namespace and sun_intensity_namespace != plant["metadata"]["namespace"]:
                    # Move the plant to the appropriate namespace
                    plant=move_plant_to_namespace(plant, sun_intensity_namespace)
                else:
                    logger.info ("No sun intensity label have to be updated for plant %s", plant['metadata']['name'])

                # log watered (date and amount) - water plant if LastTimeWatered is more than 24 Minutes ago
                if plant is not None: # this occurs if e.g. namespace could not be updated
                    if "lastTimeWatered" in plant.get("metadata", {}).get("annotations", {}):
                        timestamp = datetime.datetime.strptime(plant["metadata"]["annotations"]["lastTimeWatered"], "%Y-%m-%d %H:%M:%S.%f")
                        waterPlant = True if (datetime.datetime.now() - timestamp).total_seconds() / 60 > 24 else False
                    else:
                        waterPlant = True
                        logger.info ("plant will be watered for the first time")   
                    # Update the lastTimeWatered annotation if last watered is more than 24 minutes (h) ago
                    if waterPlant:
                        plant["metadata"]["annotations"]["lastTimeWatered"] = str(datetime.datetime.now())
                        try:
                            custom_api.replace_namespaced_custom_object(
                                group=GROUP,
                                version=VERSION,
                                namespace=plant["metadata"]["namespace"],
                                plural=PLURAL,
                                name=plant["metadata"]["name"],
                                body=plant
                            )
                            logger.info ("plant is watered") 
                        except Exception as e:
                            logger.error ("Kubernetes plant CRD annotation 'lastTimeWaterd' could not be updated / Error: %s",e) 
                    else:  
                        logger.info ("plant is not watered") 
            


if __name__ == "__main__":
    main()