# -*- coding: utf-8 -*-

import pulumi
import pulumi_digitalocean as do

# Create a new container registry
foobar = do.ContainerRegistry("foobar",
    name="python-bootcamp-foobar-73549", # name must be globally unique (so if any other digitcalocean user already created a registry with a name you cannot use the same name)
    subscription_tier_slug="starter")
