# -*- coding: utf-8 -*-

type Cadena = list | tuple | range | set | frozenset
print ("Hallo welt")

def get_diff_elements_between_list(l1: Cadena, l2: Cadena) -> list:
    """Task01
    Write a function which takes two Cadena objects and returns all values which are not in both objects
    There should be no duplicates in the result. 

    Args:
        l1 (Cadena): first Cadena object
        l2 (Cadena): second Cadena object

    Returns: 
        list: a list which contains all elements of l1 and l2 which are either in one of them but not in both
    """
    res=list(set(l1)-set(l2))+list(set(l2)-set(l1))
    return res
    ...


def search_by_ingredient(recipes: dict) -> dict:
    """Task02
    Iterate over the ingredients of different recipes. Create a new dict which contains each found ingredient as a key and a list of recipes which use this ingredient
    Example result:
    Input:
    {
        'apple pie': ['apple', 'flour'],
        'fruit salad': ['apple', 'banana'],
        'pudding': ['milk', 'starch', 'choco']
    }
    Output: 
    {
        'apple': ['apple pie', 'fruit salad'] 
        'milk': ['pancakes', 'pudding', 'hot choco']
    }

    Args:
        recipes (dict): recipes with lists of ingriedients

    Returns
        dict: by ingredient searchable dict
    """
    ingriedients_dict = {}
    flattened_list = set([item for sublist in list(recipes.values()) for item in sublist])
    ingriedients_dict = {key: [] for key in flattened_list}
    ###Achtung nicht dict.fromkeys(flattened_list, []) erzeugt deneselben array für alle keys
    print (ingriedients_dict)
    for i in flattened_list:
        for r in list(recipes.keys()):
            if i in recipes[r]:
                ingriedients_dict[i].append(r)

    print (ingriedients_dict)
    return ingriedients_dict
    ...


def switch_keys_with_values(input: dict) -> dict:
    """Task03
    Write a function that switches a the keys with the values of a dict. Example:
    {                {
      'a': 1,          1: 'a',
      'b': 2,    ->    2: 'b',
      'c': 3           3: 'c'
    }                }
    Hint 1: It might happen that a value is a mutable container, which is not hashable. Make sure to convert it to its immutable container pendant
    Hint 2: You can ignore nested structures

    Args:
        input(dict): dict of which keys and values will be switched

    Returns: 
        dict: a dict which has all the values of the input dict as keys and all keys as values
    """
    res_dict = {}
    for w in input.keys():
        if "list" in str(type(input[w])) or "dict" in str(type(input[w])) or "set" in str(type(input[w])):
            input[w] = hash(tuple(input[w]))   
    res_dict = {input[key]: key for key in input.keys()}
    print (res_dict)
    return res_dict
    ...


def append_to_immutable(input: frozenset | tuple, new_item: any) -> frozenset | tuple:
    """Task04
    Appends an item to an immutable container
    Args:
        input (frozenset | tuple): immutable container which must be extended
        new_item (any): any item which can be added to a container object

    Returns: 
        frozenset | tuple: the same type as the variable "input" has. The result is the input with the new_item added/appended
    """
    if type(input) is tuple:
        return input+tuple([new_item])    
    elif type(input) is frozenset:
        return input.union(frozenset([new_item]))
    else:
        return False
    
    ...


def dict_deepcopy(input: dict) -> dict:
    """Task05 (Optional)
    Implement your own deepcopy function to make a copy of full a dictionary
    Do not use the deepcopy() function from the copy package!

    Args:
        input (dict): dict of which a deepcopy will be created

    Returns: 
        dict: new dict object which has no references to the input dict
    """
    res={}
    for k,v in input.items():
        if isinstance(v, dict):
            v= dict_deepcopy(v)
        elif isinstance(v, list):
            v=[dict_deepcopy(i) if isinstance(i,dict) else i for i in v]
        res[k]=v
    return res
    ...
