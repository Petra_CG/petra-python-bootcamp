# -*- coding: utf-8 -*-

def untyped_function(value1: list[int|float]|tuple[int|float], value2: int|float=-1)-> int|float:
    """Task10
    Add typing annotations. Think about which types are possible and be as unrestrictive as possible without using 'any'
    Complete this docstring
    """

    x = 0
    for i in value1:
        x += i
    if x < 0:
        x *= value2
    return x

   
def strings_to_numbers(input: tuple[str]) -> list:
    """Task11
    Each item from input is checked if it can be converted to a built-in number type and added to a list. 
    If not, then None should be added to the return list. 
    All converted values will be returned in a list. 

    Hint: Do not over engineer this task :)

    Args:
        input (tuple[str])

    Returns:
        list: all converted numbers, or None for non numeric string items
    """
    res=[]
    for strvar in input:
        try:
            numeric_value = int(strvar)
            res.append(numeric_value)
        except:
            try:
                numeric_value = float(strvar)
                res.append(numeric_value)
            except:
                try:
                    numeric_value = complex(strvar)
                    res.append(numeric_value)
                except:
                    res.append(None)

    return res
    ...

type Foo = tuple[str] | list[str]
type Bar = dict[str,Foo]
type Baz = list[str] | None
def define_types_for_this_function(x:Foo, y:Bar) -> Baz:
    """Task12
    Create the custom types Foo, Bar and Baz. 
    Foo: is a type of either a list or tuple strings
    Bar: should describe a dict type which has strings as keys and Foo as possible value type
    Baz: is either a list of strings or None
    The parameter x is of type Foo, the parameter y of type Bar and the function returns an object of type Baz
    The function checks each item from x if it is a key in y. If so, concatenate the values
    Add type annotations to the function

    Args:
        x (Foo): 
        y (Bar): 
    Returns: 
        Baz: concatenated string
    """
    
    bazvar=[]
        
    for strval in x:
        if strval in y.keys():
            bazvar.append(strval)
    bazvar = None if len(bazvar) == 0 else bazvar
    return bazvar
    #return Baz(bazvar)

    ...

