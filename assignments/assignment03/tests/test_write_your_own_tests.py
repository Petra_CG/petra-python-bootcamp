# -*- coding: utf-8 -*-

### Write your test cases for the imported functions from assignment03 here. 
### You can use either doctest, unittest or pytest as testing framework

import doctest
import unittest
from assignment03.king_cobra import cal_kuh_lator, d_and_d_spell_book, query_db, query_book_metadata

class TestKingCobra(unittest.TestCase):

    def test_cal_kuh_lator(self):
        testvalues=[1,200,0,-4,0.00001]
        resultvalues=["muh", "muuuh", "möh", "hum", "mh"]
        testresult = [cal_kuh_lator(i) for i in testvalues]
        assert testresult == resultvalues

    def test_d_and_d_spell_book(self):
        testvalues=[("stereo",100,"Fritz Gehörlos"), ("cataracta",9,"Mike"),("ignis pila",88,"Tina")]
        resultvalues=["cast chill house music at volume level 100 from 2 sides at Fritz Gehörlos","cast waterfall level 9 at Mike","cast fireball level 88 at Tina"]
        testresult = [d_and_d_spell_book(name, level, target)() for name, level, target in testvalues]
        #equivalent:
        #testresult = [d_and_d_spell_book(*parameters)() for parameters in testvalues]
        assert testresult == resultvalues
        with self.assertRaises(KeyError): # wirft nur dann einen Fehler, wenn KEIN KeyError auftritt
            d_and_d_spell_book("gibt es nicht",88,"Peter")()

    def test_query_db(self):
        with self.assertRaises(Exception): 
            query_db()

    def test_query_book_metadata(self): 
        with self.assertRaises(Exception): 
            query_book_metadata("978-3-427-09560-6")    