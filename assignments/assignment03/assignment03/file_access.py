# -*- coding: utf-8 -*-

import csv
import json
from pathlib import Path
import os

"""
HINT: For these assignments the unittests might also contain the solution. So only have a look into them if you really got stuck
"""


def csv_to_json():
    """Task01
    Read in the cve file 'userdata.cve' in the data folder
    Create a dict from the data where the first column "username" is the key and the other columns are the value in dict format such that the column name is the key
    Write the dict as json in a new file called 'userdata.json' in the same folder 

    Hint: running the unittests of this assignment will reset the data directory before and after the tests.

    Writes the file assignment03/data/userdata.json

    Returns:
        none

    """
    importfile_pathcsv = Path(__file__).parent.parent / "data" / "userdata.csv" # damit man das Modul von beliebiger Stelle starten kann
    exportfile_pathjson  = Path(__file__).parent.parent / "data" / "userdata.json"
    #importfile_pathcsv  = Path(".") / "data" / "userdata.csv"
    #exportfile_pathjson  = Path() / "data" / "userdata.json"
    content = importfile_pathcsv.read_text(encoding="utf-8")
    contentLineArray = content.splitlines()
    userValueKeys = contentLineArray[0].split(",") #e.g. 'email', 'country', 'account_created'
    resultDict={}
    
    #ContentArrayx= ContentLineArray.split(",")
    for dataline in contentLineArray[1:]: # without headline
        #print("dataline",dataline)
        datasplit=dataline.split(',')
        userinfoDict={}
        for userinfoid, userinfo in enumerate(datasplit):
            if userinfoid == 0:
               continue
            #print ("Userinfo",userinfo)
            userinfoDict[userValueKeys[userinfoid]]=userinfo
        resultDict[datasplit[0]]=userinfoDict
    #print (resultDict)
    #serialized_result = json.dumps(resultDict)  # no indentation or new-lines
    serialized_result = json.dumps(resultDict, ensure_ascii=False, indent=4)
    exportfile_pathjson.write_text(serialized_result)
    #with exportfile_pathjson.open("w", encoding="utf-8") as target:
    #    json.dump(resultDict, target, ensure_ascii=False, indent=4)
    ...

def fun_with_paths() -> tuple[Path]:
    """Task02
    Use the python pathlib library to get information from the file system

    Returns: 
      path: the absolute path of this module, e.g. "C:\\python-bootcamp\\assignments\\assignment03\\assignment03\"
      path: the relative path from this module to lecture03.pptx, e.g. "..\\..\\lectures\\lecture03.pptx"
      path: the combined path from the previous two, e.g. "C:\\python-bootcamp\\assignments\\assignment03\\assignment03\\..\\..\\lectures\\lecture03.pptx"
    """

    abs_module = Path(__file__)     # Path(__file__).parent # ohne Dateinamen, nur Pfad
    print("abs_module", abs_module)
    rel_lecture = Path("..") / ".." / "lectures" / "lecture03.pptx"
    print("rel_lecture", rel_lecture)
    combined_lecture = abs_module / rel_lecture
    print("combined_lecture", combined_lecture)
    abs_lecture = combined_lecture.resolve()
    print("abs_lecture", abs_lecture)
    return abs_module, rel_lecture, combined_lecture
    # ----

    current_file_name =__file__
    print("current filename",__file__)
    absolute_path = os.path.abspath(os.path.dirname(__file__))
    rel_path = os.path.relpath(__file__)  # , start=os.curdir
    basename_path = os.path.basename(__file__)
    dirname_path = os.path.dirname(__file__)
    print("absolute_os",absolute_path)
    print ("absolute2",Path(__file__).absolute())
    print ("absolute3*",Path(__file__).parent)
    print("dirname ",dirname_path)
    print("rel path",rel_path)
    print("basename",basename_path)
    
    print("current working directory",os.getcwd())
    print(os.path.dirname(rel_path))
    #print(sys.path) path systemvariable
    relative_diffpath = os.path.relpath("lectures", start=os.path.dirname(rel_path))
    print("rel_diffpath",relative_diffpath)
    relative_combinedpath = os.path.dirname(rel_path)+"\\"+relative_diffpath
    print("combined",relative_combinedpath)
    assignment_dir=dirname_path+"\\..\\.."
    print("dirlist",os.listdir(assignment_dir))
    
 
    ...


def fun_with_paths_part2() -> tuple[list[Path], set[str]]:
    """Task03
    Use the python pathlib library to get information from the file system

    Returns:
      list[path]: a list of all files (including their absolut path) in this assignemt03 directory which do not have the .py suffix (do not include the __pycache__ directory!)
      set[str]: a set of all file endings of the found files, e.g. [".yaml", ".json"] - Hint: no empty strings
    """
    dir = Path(__file__).parent.parent
    # nur aktuelles verzeichnis (NICHT REKURSIV), nur .md dateien
    #for file in dir.glob("*.md"):
    #   print(file)

    files = []
    for file in dir.rglob("*"):
       if file.suffix == ".py":
          continue
       if any(parent.name == "__pycache__" for parent in file.relative_to(dir).parents):
          continue
       files.append(file)
    suffixes = set(file.suffix for file in files) - {""}   # {""} entspricht set([""])
    print(suffixes)
    return files, suffixes
    files = [file for file in dir.iterdir() if file.suffix != ".py" and file.name != "__pycache__"]
    for file in files:
       print(file)

    ...


def implement_walk_function(path: Path) -> list[tuple[Path, list[str], list[str]]]:
    """Task05 (optional)
    Implement the python walk function: https://docs.python.org/3/library/pathlib.html#pathlib.Path.walk
    Parameters of the walk function can be ignored 
    """
    s_dirs = [dir.name for dir in path.iterdir() if dir.is_dir()]
    s_files = [file.name for file in path.iterdir() if file.is_file()]
    result = [(path, s_dirs, s_files)]
    for dir in s_dirs:
        #result.extend(implement_walk_function(path / dir))
        # equivalent
        result += implement_walk_function(path / dir)
    return result
